import pywapi

from flask import Flask, request
from flask.views import MethodView

app = Flask(__name__)

@app.errorhandler(500)
def internal_error(error):
    return "Cannot retrieve what you are looking for right now"

class Request(MethodView):
    def post(self):
        outcome = request.json['outcome']
        location="Paris"#Default
        if 'entities' in outcome:
            if 'location' in outcome['entities']:
                if len(outcome['entities']['location']) > 0:
                    if 'value' in outcome['entities']['location'][0]:
                        tmp_location = outcome['entities']['location'][0]['value']
                        if tmp_location != "outside":
                            location = tmp_location
        location_id = ""
        locations_ids=pywapi.get_loc_id_from_weather_com(location)
        if locations_ids['count'] > 0:
            location_id = locations_ids[0][0]
        if location_id != "":
            weather = pywapi.get_weather_from_weather_com(location_id)
            if request.json['lang'] == 'fr':
                answer = "Il fait " + weather['current_conditions']['temperature'] + " degrees celsius en ce moment à " + location
            elif request.json['lang'] == 'en':
                answer = "The temperature is " + weather['current_conditions']['temperature'] + " degrees celsius, now in " + location
        else:
            if request.json['lang'] == 'fr':
                answer = "Je ne trouve pas ce que vous cherchez"
            elif request.json['lang'] == 'en':
                answer = "I can't find what you are looking for"
        return answer

if __name__ == '__main__':
    app.add_url_rule('/', view_func=Request.as_view('request'))
    app.run(host='0.0.0.0', port=80)
